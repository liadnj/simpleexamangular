import { SimpleExamPage } from './app.po';

describe('simple-exam App', function() {
  let page: SimpleExamPage;

  beforeEach(() => {
    page = new SimpleExamPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
