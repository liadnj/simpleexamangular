import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 

@Injectable()
export class ProductsService {

products;
constructor(private af:AngularFire) { }
/*products = [ ///localdata
    {name:'John',cid:'1'},
    {name:'alis',cid:'2'},
    {name:'Alice',cid:'3'}
  ]


  getProducts(){
		return this.products;
	}*/

  deleteProduct(product){
    this.af.database.object('/product/' + product.$key).remove();
    console.log('/product/' + product.$key);
  }

  getProducts(){
    this.products = this.af.database.list('/product');
    return this.products;
	}

}
