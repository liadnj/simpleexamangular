import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';

import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';

import { CategoryComponent } from './category/category.component';
import { CategoryService } from './category/category.Service';
import { InvoiceFormComponentComponent } from './invoice-form-component/invoice-form-component.component';
import {InvoiceFormComponentService } from './invoice-form-component/invoice-form-component.service';


import { InvoicesComponentComponent } from './invoices-component/invoices-component.component';
import { InvoicesComponentService } from './invoices-component/invoices-component.service';

import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';


export const firebaseConfig = {
 apiKey: "AIzaSyAWQbdwSHm1aJbcwBuH9kFZaOSvo9Go-4g",
    authDomain: "examliad.firebaseapp.com",
    databaseURL: "https://examliad.firebaseio.com",
    storageBucket: "examliad.appspot.com",
    messagingSenderId: "657095264655"
}

const appRoutes: Routes = [
 { path: 'invoice-form-component', component: InvoiceFormComponentComponent },
 { path: 'invoices-component', component: InvoicesComponentComponent },
 { path: '', component: InvoiceFormComponentComponent },
 { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    CategoryComponent,
    InvoiceFormComponentComponent,
    InvoicesComponentComponent,
    PageNotFoundComponent,
    InvoiceComponent,
    SpinnerComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ProductsService,CategoryService,InvoicesComponentService,InvoiceFormComponentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
