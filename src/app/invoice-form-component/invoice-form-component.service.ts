import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoiceFormComponentService {
invoiceObservable;
 constructor(private af:AngularFire) { }
  
  addInvoice(invoice){
    this.invoiceObservable.push(invoice);
  }


  getUsers(){
    this.invoiceObservable = this.af.database.list('/invoice');
    return this.invoiceObservable;
	}
}