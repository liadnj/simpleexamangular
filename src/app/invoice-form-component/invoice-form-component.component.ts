import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {invoiceformcomponent} from './invoice-form-component';
import {InvoiceFormComponentService} from './invoice-form-component.service';


@Component({
  selector: 'jce-invoice-form-component',
  templateUrl: './invoice-form-component.component.html',
  styleUrls: ['./invoice-form-component.component.css']
})
export class InvoiceFormComponentComponent implements OnInit {
@Output() invoiceAddedEvent = new EventEmitter<invoiceformcomponent>();

 invoice:invoiceformcomponent = {
    name: '',
    amount: ''
  }
  
 
isLoading = true;

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {
        name: '',
    amount: ''
    }
  }

  ngOnInit() {
  }
 }