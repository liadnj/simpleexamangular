/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InvoiceFormComponentService } from './invoice-form-component.service';

describe('InvoiceFormComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoiceFormComponentService]
    });
  });

  it('should ...', inject([InvoiceFormComponentService], (service: InvoiceFormComponentService) => {
    expect(service).toBeTruthy();
  }));
});
