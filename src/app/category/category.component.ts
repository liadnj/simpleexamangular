import { Component, OnInit } from '@angular/core';
import { CategoryService } from './category.Service';


@Component({
  selector: 'jce-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  styles: [`
    .category li { cursor: default; }
    .category li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class CategoryComponent implements OnInit {
products;
   constructor(private _categoryService: CategoryService) {}

  ngOnInit() {
    this._categoryService.getProducts()
			    .subscribe(products => {this.products = products;})
  }

}
