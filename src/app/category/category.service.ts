import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';


@Injectable()
export class CategoryService {
  products;
  productsObservable;
constructor(private af:AngularFire) { }

/*
//// get function before join
getProducts(){
    this.products = this.af.database.list('/product');
    return this.products;
	}*/

  getProducts(){

   this.productsObservable = this.af.database.list('/product').map(
     products =>{
       products.map(
         product => {
           product.categoryName = [];
           for(var p in product.cid){
               product.categoryName.push(
               this.af.database.object('/category/' + p)
             )
           }
         }
      );
       return products;
     }
   )
   //this.usersObservable = this.af.database.list('/users');
     return this.productsObservable;
 	}
}
