/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InvoicesComponentService } from './invoices-component.service';

describe('InvoicesComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoicesComponentService]
    });
  });

  it('should ...', inject([InvoicesComponentService], (service: InvoicesComponentService) => {
    expect(service).toBeTruthy();
  }));
});
