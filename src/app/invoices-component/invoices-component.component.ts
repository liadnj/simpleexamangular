import { Component, OnInit } from '@angular/core';
import {InvoicesComponentService} from './invoices-component.service';

@Component({
  selector: 'app-invoices-component',
  templateUrl: './invoices-component.component.html',
  styleUrls: ['./invoices-component.component.css']
})
export class InvoicesComponentComponent implements OnInit {

  invoices;

 currentInvoice;

 isLoading = true;

  constructor(private _invoicescomponentcservice: InvoicesComponentService) {
    //this.users = this._userService.getUsers();
  }
 select(invoice){
		this.currentInvoice = invoice; 
    //console.log(	this.currentUser);
 }
  
  
  addInvoice(invoice){
    this._invoicescomponentcservice.addInvoice(invoice);
  }

   
  


  ngOnInit() {
        this._invoicescomponentcservice.getInvoices()
			    .subscribe(invoices => {this.invoices = invoices;
                               this.isLoading = false;
                               console.log(invoices)});
  }

}