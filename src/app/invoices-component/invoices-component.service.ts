import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesComponentService {
invoiceObservable;


constructor(private af:AngularFire) { }
  
  addInvoice(invoice){
    this.invoiceObservable.push(invoice);
  }


  getInvoices(){
    this.invoiceObservable = this.af.database.list('/invoices');
    return this.invoiceObservable;
	}
}
 


